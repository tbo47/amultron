import { AMuleCli, AmuleResponse } from '../src/app/amule';
const md5 = require('../node_modules/blueimp-md5/js/md5.js');
const StringDecoder = require('string_decoder').StringDecoder;

/**
 * This node script refreshes the shared files list and print as html
 *
 *
 * Run
 * > npm install
 * > npm run export > my-shared-files.html
 *
 */

const aMule = new AMuleCli('127.0.0.1', 4712, 'password', md5);

aMule.setStringDecoder(new StringDecoder('utf8'));

console.log('<html><body>');

aMule.connect()
  .then(() => aMule.reloadSharedFiles())
  .then(() => aMule.getSharedFiles())
  .then(res => getMostUsedWord(res))
  .then(res => logList(res))
  .then(() => process.exit(0));

/**
 * Print list in html format
 */
function logList(list: AmuleResponse[]) {
  console.log('<div>%i files are shared</div>', list.length);
  list.map(e => console.log('<a href="%s">%s</a><br>', e.partfile_ed2k_link, e.partfile_name));
  console.log('</body></html>');
  return list;
}

/**
 * get the most common terms
 */
function getMostUsedWord(list: AmuleResponse[]) {
  console.log(aMule.getMostUsedWord(list));
  return list;
}
