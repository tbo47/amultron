import aMuleModule = require('../src/app/amule');
const md5 = require('../node_modules/blueimp-md5/js/md5.js');
const StringDecoder = require('string_decoder').StringDecoder;

/**
 *
 * This node script searches for a keywork including the current date (ex: "linux 2017-06")
 * and download the results list.
 *
 *
 * Run
 * > npm install
 * > npm run search
 *
 */

const aMule = new aMuleModule.AMuleCli('127.0.0.1', 4712, 'password', md5);

aMule.setStringDecoder(new StringDecoder('utf8'));

const query = 'clo2 ' + aMule.getMonth();

const showSearchList = list => {
    console.log('%d results found and downloaded for query: %s', list.length, query);
    list.map(e => console.log(e['partfile_name']));
};

aMule.connect()
    .then(m => aMule.search(query, 2)) // trigger a search
    .then(result => result.map(e => () => aMule.download(e))) // create a array of function to download each result
    .then(funcs => aMule.promiseSerial(funcs)) // trigger the downloads sequencially
    .then(showSearchList)
    .then(() => process.exit(0));
