import { Pipe, PipeTransform } from '@angular/core';

/**
 * format a file size in a human readable way.
 */
@Pipe({ name: 'fileSize' })
export class FileSizePipe implements PipeTransform {
    transform(text: number) {

        function format(bytes: number, si: boolean) {
            const thresh = si ? 1000 : 1024;
            if (Math.abs(bytes) < thresh) {
                return bytes + ' B';
            }
            const units = si
                ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
                : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
            let u = -1;
            do {
                bytes /= thresh;
                ++u;
            } while (Math.abs(bytes) >= thresh && u < units.length - 1);
            return bytes.toFixed(1) + ' ' + units[u];
        }

        return format(text, true);
    }
}
