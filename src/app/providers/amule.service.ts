import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { merge, Observable, from, of } from 'rxjs';
import { map, mergeMap, filter, scan, tap, toArray, concatMap, catchError } from 'rxjs/operators';
import { AMuleCli, AmuleResponse } from '../amule';
import { EdkFile } from './file';

const md5 = require('blueimp-md5');
const { shell } = require('electron');
const StringDecoder = require('string_decoder').StringDecoder;

export interface AmuleData {
  dls: AmuleResponse[];
  res: AmuleResponse[];
}

export class Suggestion {
  name: string;
  results: AmuleResponse[];
  shortList: AmuleResponse[];
  downloaded: boolean;
  currentDl: boolean;
  constructor(name: string) {
    this.name = name;
  }
}

export class AmultronAuth {
  url = 'localhost';
  port = 4712;
  password = 'password';
  autoconnect = false;
}

@Injectable()
export class AmuleService {
  private aMule: AMuleCli;
  private isBusy = false;

  constructor(public snackBar: MatSnackBar, protected storage: LocalStorage) {}

  create(params: AmultronAuth) {
    this.aMule = new AMuleCli(params.url, params.port, params.password, md5);
    this.aMule.setStringDecoder(new StringDecoder('utf8'));
  }

  /**
   *
   * @param data dl and currently dl lists
   */
  getListsFromAmule(data: AmuleData): Promise<AmuleData> {
    if (this.isBusy) {
      console.log('update skipped');
      return Promise.resolve(data);
    }
    data = { res: [], dls: [] };
    this.isBusy = true;
    return this.aMule
      .connect()
      .then(() => this.aMule.clearCompleted())
      .then(() => this.aMule.reloadSharedFiles())
      .then(() => this.aMule.getSharedFiles())
      .then(res => (data.res = res))
      .then(() => this.aMule.getDownloads())
      .then(res => (data.dls = res))
      .then(() => {
        this.isBusy = false;
        return data;
      })
      .catch(err => {
        this.isBusy = false;
        throw err;
      });
  }

  getLists(data: AmuleData): Observable<AmuleData> {
    return from(this.getListsFromAmule(data)).pipe(
      mergeMap(amuleData => {
        return from(amuleData.res).pipe(
          mergeMap(r => {
            return this.populateWithLocalData(r);
          }),
          toArray(),
          map(res2 => {
            amuleData.res = res2;
            console.log(amuleData);
            return amuleData;
          }),
        );
      }),
    );
  }

  search(q: string, n: number): Observable<EdkFile[]> {
    this.isBusy = true;
    console.log('start searching for %s', q);
    return from(this.aMule.search(q, n)).pipe(
      mergeMap(rawList => {
        rawList = rawList.sort((a, b) => b.partfile_source_count - a.partfile_source_count);
        console.log('number of results for %s:  %i', q, rawList.length);
        return from(rawList).pipe(
          mergeMap(res => {
            return this.populateWithLocalData(res);
          }),
        );
      }),
      toArray(),
      tap((list: EdkFile[]) => {
        this.isBusy = false;
        list.forEach(e => {
          e.color = e.currentDl ? 'green' : '';
          e.color = e.downloaded ? 'darkgreen' : e.color;
        });
      }),
      catchError(err => {
        console.error('search failed: %s', q);
        console.error(Date().toString() + err);
        this.isBusy = false;
        return of([]);
      }),
    );
  }

  snack(msg: string) {
    this.snackBar.open(msg, '', { duration: 2000 });
  }

  download(e: EdkFile): Promise<any> {
    if (e.downloaded) {
      this.snack('Already downloaded');
    } else if (e.currentDl) {
      this.snack('Currently being downloaded');
    } else {
      e.currentDl = true;
      this.snack('Added to the download queue');
      return this.aMule.download(e);
    }
    return Promise.resolve();
  }

  open(e: AmuleResponse) {
    const n = e.knownfile_filename + '/' + e.partfile_name;
    this.snack('Opening file ' + e.partfile_name);
    console.log(n);
    shell.openItem(n);
  }

  /**
   * get the date. Example: "2017-11"
   */
  public getMonth(): string {
    const dateObj = new Date(),
      month = dateObj.getUTCMonth() + 1,
      monthStr: string = ('00' + month).slice(-2),
      year: number = dateObj.getUTCFullYear();
    return year + '-' + monthStr;
  }

  /**
   * get info from localstorage for an element
   *
   * @param list search list
   */
  populateWithLocalData(res: AmuleResponse): Observable<EdkFile> {
    return this.storage.getItem<EdkFile>(res.partfile_hash).pipe(
      mergeMap(file => {
        if (file) {
          // the file is already in the local storage
          const cachedFile = file as EdkFile;
          const amuleFile = res as EdkFile;
          amuleFile.firstSeen = cachedFile.firstSeen;
          amuleFile.firstSeenDl = cachedFile.firstSeenDl;
          if (res.downloaded && !cachedFile.firstSeenDl) {
            // if a file is newly downloaded
            cachedFile.firstSeenDl = new Date();
            cachedFile.downloaded = true;
            return this.storage.setItem(res.partfile_hash, cachedFile).pipe(map(() => cachedFile));
          }
          return of(amuleFile);
        } else {
          // transform an AmuleResponse in EdkFile and store it in local storage
          const f = res as EdkFile;
          f.firstSeen = new Date();
          if (res.downloaded) {
            f.firstSeenDl = new Date();
          }
          return this.storage.setItem(res.partfile_hash, res).pipe(map(() => f));
        }
      }),
    );
  }

  public getDefaultSearch(): Observable<string> {
    return this.storage.getItem('default-search').pipe(
      map(q => {
        let c = q as string;
        if (!c) {
          c = this.getMonth();
          this.setDefaultSearch(c);
        }
        return c;
      }),
    );
  }

  public setDefaultSearch(p: string): void {
    return this.storage.setItemSubscribe('default-search', p);
  }

  public getAuthFromCache(): Observable<AmultronAuth> {
    return this.storage.getItem('amultron-auth').pipe(
      map(conf => {
        let c = conf as AmultronAuth;
        if (!c) {
          c = new AmultronAuth();
          this.setAuthInCash(c);
        }
        return c;
      }),
    );
  }

  public setAuthInCash(p: AmultronAuth): void {
    return this.storage.setItemSubscribe('amultron-auth', p);
  }

  public getSuggestions(dls: AmuleResponse[]): Observable<Suggestion[]> {
    return this.storage.getItem('amultron-suggestions').pipe(
      map(s => {
        let c = s as Suggestion[];
        if (!c) {
          c = this.getMostUsedWord(dls);
          this.setSuggestionsInCash(c);
        }
        return c;
      }),
    );
  }

  public setSuggestionsInCash(s: Suggestion[]): void {
    return this.storage.setItemSubscribe('amultron-suggestions', s);
  }

  public getMostUsedWord(list: AmuleResponse[], limit = 10, minLength = 4): Suggestion[] {
    return this.aMule.getMostUsedWord(list, limit, minLength).map(r => new Suggestion(r));
  }

  /**
   * trigger amule search one after one another and return a array of suggestions with results in it.
   * @param suggestions array of search terms
   */
  public triggerSuggestionSearch(suggestions: Suggestion[], n: number): Observable<Suggestion[]> {
    return from(suggestions).pipe(
      concatMap(q =>
        this.search(q.name, n).pipe(
          map(l => {
            q.results = l.sort((a, b) => b.firstSeen.getTime() - a.firstSeen.getTime());
            return q;
          }),
        ),
      ),
      toArray(),
    );
  }
}
