import { AmuleResponse } from '../amule';

/**
 * File is an AmuleResponse with extra parameters which comes from amultron usage.
 */
export class EdkFile extends AmuleResponse {
  public firstSeen: Date; // Date when the file was first seen in a search
  public firstSeenDl: Date; // Date first seen downloaded
  public color: string; // css color in the html list
}
