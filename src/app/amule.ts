const net = require('net'); // nodejs network library

export class AmuleResponse {
  public partfile_hash: string; // uniq id in the edk network
  public header: number;
  public totalSizeOfRequest = 0;
  public opCode = null;
  public typeEcOp: number;
  public tagCountInResponse: number;
  public opCodeLabel: string;
  public children: AmuleResponse[] = [];
  public nameEcTag: number;
  public value: any;
  public length: number;
  public sourceCount = 0;
  public sourceCountXfer = 0;
  public status = 0;
  public partfile_source_count: number;
  public partfile_size_full: number;
  public partfile_name: string;
  public downloaded = false; // set to true for files in the downloaded list
  public currentDl: boolean; // is the search result in the download queue already
  public completeness: number;
  public sharedRatio: number;
  public knownfile_filename: string;
}

export class AMuleCli {
  public isConnected: Boolean = false;
  private offset = 0; // use internally to read bit stream from server
  private arrayBuffers = []; // used to build requests
  private recurcifInBuildTagArrayBuffer = 0;
  private responseOpcode = 0; // op code given in the server

  private ip: string; // server address
  private port: number; // server port
  private md5Password: string; // md5 value of the password
  private solt = ''; // solt number (sessions id)
  private md5Function; // function to md5()
  private textDecoder;
  private stringDecoder;
  private client; // node socket

  private ECCodes = {
    EC_CURRENT_PROTOCOL_VERSION: 0x0204,
    EC_OP_AUTH_REQ: 0x02,
    EC_OP_GET_SHARED_FILES: 0x10,
  };
  private ECOpCodes = {
    EC_OP_STRINGS: 0x06,
    EC_TAGTYPE_UINT16: 0x03,
    EC_TAGTYPE_CUMSTOM: 1,
    EC_TAGTYPE_UINT8: 2, // defined in ECTagTypes.h
    EC_TAGTYPE_HASH16: 0x09,
    EC_OP_AUTH_FAIL: 0x03,
    EC_OP_AUTH_OK: 0x04,
    EC_OP_SEARCH_START: 0x26,
    EC_OP_SEARCH_STOP: 0x27,
    EC_OP_SEARCH_RESULTS: 0x28,
    EC_OP_SEARCH_PROGRESS: 0x29,
    EC_OP_DOWNLOAD_SEARCH_RESULT: 0x2a,
  };
  private ECTagNames = {
    EC_TAG_CLIENT_NAME: 0x0100,
    EC_TAG_CLIENT_VERSION: 0x0101,
    EC_TAG_PROTOCOL_VERSION: 0x0002,
    EC_TAG_PASSWD_HASH: 0x0001,
  };
  private ProtocolVersion = {
    EC_CURRENT_PROTOCOL_VERSION: 0x0204,
  };
  private EC_SEARCH_TYPE = {
    EC_SEARCH_LOCA: 0x00,
    EC_SEARCH_GLOBAL: 0x01,
    EC_SEARCH_KAD: 0x02,
    EC_SEARCH_WEB: 0x03,
  };
  private EC_TAG_SEARCHFILE = {
    EC_TAG_SEARCH_TYPE: 0x0701, // 1793/2?
    EC_TAG_SEARCH_NAME: 0x0702,
    EC_TAG_SEARCH_MIN_SIZE: 0x0703,
    EC_TAG_SEARCH_MAX_SIZE: 0x0704,
    EC_TAG_SEARCH_FILE_TYPE: 0x0705,
    EC_TAG_SEARCH_EXTENSION: 0x0706,
    EC_TAG_SEARCH_AVAILABILITY: 0x0707,
    EC_TAG_SEARCH_STATUS: 0x0708,
    EC_TAG_SEARCH_PARENT: 0x0709,
  };

  private EC_TAG_MAPPING = [
    { EC_TAG_PARTFILE_NAME: 769 },
    { EC_TAG_PARTFILE_SIZE_DONE: 774 },
    { EC_TAG_PARTFILE_SPEED: 775 },
    { EC_TAG_PARTFILE_LAST_SEEN_COMP: 785 },
    { EC_TAG_PARTFILE_LAST_RECV: 784 },
    { EC_TAG_PARTFILE_COMMENTS: 790 },
    { EC_TAG_PARTFILE_HASH: 798 },
    { EC_TAG_PARTFILE_SIZE_FULL: 771 },
    { EC_TAG_PARTFILE_ED2K_LINK: 782 },
    { EC_TAG_PARTFILE_SOURCE_COUNT: 778 },
    { EC_TAG_PARTFILE_SOURCE_COUNT_XFER: 781 },
    { EC_TAG_PARTFILE_STATUS: 776 },
    { EC_TAG_KNOWNFILE_FILENAME: 1032 },
    { EC_TAG_KNOWNFILE_XFERRED_ALL: 1026 },
    { EC_TAG_KNOWNFILE_REQ_COUNT_ALL: 1028 },
    { EC_TAG_PARTFILE_LAST_SEEN_COMP: 785 },
    { EC_TAG_PARTFILE_LAST_RECV: 784 },
    { EC_TAG_PREFS_GENERAL: 4608 },
    { EC_TAG_USER_NICK: 4609 },
    { EC_TAG_USER_HASH: 4610 },
    { EC_TAG_PREFS_CONNECTIONS: 4864 },
    { EC_TAG_CONN_MAX_UL: 4868 },
    { EC_TAG_CONN_MAX_DL: 4867 },
    { EC_TAG_CLIENT: 1536 },
    { EC_TAG_CLIENT_NAME: 256 },
    { EC_TAG_CLIENT_HASH: 1539 },
    { EC_TAG_CLIENT_USER_ID: 1566 },
    { EC_TAG_CLIENT_SOFTWARE: 1537 },
    { EC_TAG_CLIENT_SOFT_VER_STR: 1557 },
    { EC_TAG_CLIENT_USER_IP: 1552 },
    { EC_TAG_CLIENT_USER_PORT: 1553 },
    { EC_TAG_CLIENT_DISABLE_VIEW_SHARED: 1563 },
    { EC_TAG_CLIENT_OS_INFO: 1577 },
    { EC_TAG_SERVER: 1280 },
    { EC_TAG_STATS_LOGGER_MESSAGE: 525 },
    { EC_TAG_STATS_TOTAL_SENT_BYTES: 536 },
    { EC_TAG_STATS_TOTAL_RECEIVED_BYTES: 537 },
    { EC_TAG_STATS_UL_SPEED: 512 },
    { EC_TAG_STATS_DL_SPEED: 513 },
    { EC_TAG_STATS_UL_SPEED_LIMIT: 514 },
    { EC_TAG_STATS_DL_SPEED_LIMIT: 515 },
    { EC_TAG_PREFS_DIRECTORIES: 6656 },
    { EC_TAG_DIRECTORIES_INCOMING: 6657 },
    { EC_TAG_DIRECTORIES_TEMP: 6658 },
  ];

  constructor(ip: string, port: number, password: string, md5Function) {
    this.ip = ip;
    this.port = port;
    this.md5Function = md5Function;
    // must be the same as ECPassword in .aMule/amule.conf
    this.md5Password = this.md5(password);
  }

  private md5(str: string) {
    return this.md5Function(str);
  }

  public setTextDecoder(textDecoder) {
    this.textDecoder = textDecoder;
  }

  public setStringDecoder(stringDecoder) {
    this.stringDecoder = stringDecoder;
  }

  /**
   * from amule ECCodes.h code
   */
  /**
   * Used internally to build a request
   */
  private _buildTagArrayBuffer(ecTag: number, ecOp: number, value, children) {
    this.recurcifInBuildTagArrayBuffer++;
    let tagLength = 0;
    let dv = new DataView(new ArrayBuffer(Uint16Array.BYTES_PER_ELEMENT));
    dv.setUint16(0, ecTag, false); // name
    this.arrayBuffers.push(dv.buffer);
    tagLength += Uint16Array.BYTES_PER_ELEMENT;

    dv = new DataView(new ArrayBuffer(Uint8Array.BYTES_PER_ELEMENT));
    dv.setUint8(0, ecOp); // type
    this.arrayBuffers.push(dv.buffer);
    tagLength += Uint8Array.BYTES_PER_ELEMENT;

    const lengthDataView = new DataView(new ArrayBuffer(Uint32Array.BYTES_PER_ELEMENT));
    this.arrayBuffers.push(lengthDataView.buffer);
    // data length is going to be set after the children are created
    tagLength += Uint32Array.BYTES_PER_ELEMENT;

    let childrenTagsLength = 0;
    if ((ecTag & 0x01) !== 0 && children === null) {
      // if tag has no child
      dv = new DataView(new ArrayBuffer(Uint16Array.BYTES_PER_ELEMENT));
      dv.setUint16(0, 0, false);
      this.arrayBuffers.push(dv.buffer);
      if (this.recurcifInBuildTagArrayBuffer < 2) {
        tagLength += Uint16Array.BYTES_PER_ELEMENT;
      }
    } else if (children) {
      // if tag has a child
      dv = new DataView(new ArrayBuffer(Uint16Array.BYTES_PER_ELEMENT));
      this.arrayBuffers.push(dv.buffer);
      tagLength += Uint16Array.BYTES_PER_ELEMENT;
      for (let m = 0; m < children.length; m++) {
        // console.log("child " + children[m].ecTag + " " + children[m].ecOp + " " + children[m].value);
        childrenTagsLength += this._buildTagArrayBuffer(children[m].ecTag, children[m].ecOp, children[m].value, null);
        // console.log("childrenTagsLength : " + childrenTagsLength);
      }
      dv.setUint16(0, children.length, false);
    }

    // set length after children are created
    if (ecOp === this.ECOpCodes.EC_TAGTYPE_UINT16) {
      // length
      lengthDataView.setUint32(0, 2 + childrenTagsLength, false);
    } else if (ecOp === this.ECOpCodes.EC_TAGTYPE_UINT8) {
      lengthDataView.setUint32(0, 1 + childrenTagsLength, false);
    } else if (ecOp === this.ECOpCodes.EC_TAGTYPE_HASH16) {
      lengthDataView.setUint32(0, value.length / 2 + childrenTagsLength, false);
    } else {
      lengthDataView.setUint32(0, parseInt(value.length + childrenTagsLength, 10), false);
    }

    // set content
    if (ecOp === this.ECOpCodes.EC_OP_STRINGS) {
      for (let i = 0; i < value.length; i++) {
        const dv1 = new DataView(new ArrayBuffer(Uint8Array.BYTES_PER_ELEMENT));
        dv1.setUint8(0, value[i].charCodeAt(0));
        this.arrayBuffers.push(dv1.buffer);
        tagLength += Uint8Array.BYTES_PER_ELEMENT;
      }
    } else if (ecOp === this.ECOpCodes.EC_TAGTYPE_UINT8) {
      const dv4 = new DataView(new ArrayBuffer(Uint8Array.BYTES_PER_ELEMENT));
      dv4.setUint8(0, value);
      this.arrayBuffers.push(dv4.buffer);
      tagLength += Uint8Array.BYTES_PER_ELEMENT;
    } else if (ecOp === this.ECOpCodes.EC_TAGTYPE_HASH16) {
      for (let i = 0; i < value.length; i = i + 2) {
        const dv2 = new DataView(new ArrayBuffer(Uint8Array.BYTES_PER_ELEMENT));
        const hashValue = parseInt(value[i] + value[i + 1], 16);
        dv2.setUint8(0, hashValue);
        this.arrayBuffers.push(dv2.buffer);
        tagLength += Uint8Array.BYTES_PER_ELEMENT;
      }
    } else if (ecOp === this.ECOpCodes.EC_TAGTYPE_UINT16) {
      const dv3 = new DataView(new ArrayBuffer(Uint16Array.BYTES_PER_ELEMENT));
      dv3.setUint16(0, value, false);
      this.arrayBuffers.push(dv3.buffer);
      tagLength += Uint16Array.BYTES_PER_ELEMENT;
    }
    this.recurcifInBuildTagArrayBuffer--;
    return tagLength;
  }

  /**
   * Build request headers
   */
  private _setHeadersToRequest(opCode: number) {
    let dv = new DataView(new ArrayBuffer(Uint32Array.BYTES_PER_ELEMENT));
    // set flags, normal === 32 (34 pour amule-gui)
    dv.setUint32(0, 32, false);
    this.arrayBuffers.push(dv.buffer);
    // packet body length, will be set at the end
    this.arrayBuffers.push(new ArrayBuffer(Uint32Array.BYTES_PER_ELEMENT));
    dv = new DataView(new ArrayBuffer(Uint8Array.BYTES_PER_ELEMENT));
    dv.setUint8(0, opCode); // op code
    this.arrayBuffers.push(dv.buffer);
    // tag count, will be set at the end
    this.arrayBuffers.push(new ArrayBuffer(Uint16Array.BYTES_PER_ELEMENT));
  }

  /**
   * Build a ArrayBuffer from the array of DataView, set body length in bytes
   * and tag count.
   *
   * @returns {ArrayBuffer}
   */
  private _finalizeRequest(tagCount): ArrayBuffer {
    // calculating the buffer length in bytes
    let bufferLength = 0;
    for (let i = 0; i < this.arrayBuffers.length; i++) {
      bufferLength = bufferLength + this.arrayBuffers[i].byteLength;
    }
    // creating ArrayBuffer with all the DataViews above
    const buffer = new ArrayBuffer(bufferLength);
    let offset = 0;
    for (let i = 0; i < this.arrayBuffers.length; i++) {
      for (let j = 0; j < this.arrayBuffers[i].byteLength; j++) {
        const fromArrayView = new Uint8Array(this.arrayBuffers[i], j, 1);
        const toArrayView = new Uint8Array(buffer, j + offset, 1);
        toArrayView.set(fromArrayView);
      }
      offset = offset + this.arrayBuffers[i].byteLength;
    }
    this.arrayBuffers = [];
    // set body length
    const bodyLengthDataView = new DataView(buffer, Uint32Array.BYTES_PER_ELEMENT, Uint32Array.BYTES_PER_ELEMENT);
    bodyLengthDataView.setUint32(0, buffer.byteLength - Uint32Array.BYTES_PER_ELEMENT * 2, false);
    // console.log('> body length: '+ (buffer.byteLength - Uint32Array.BYTES_PER_ELEMENT * 2));
    // set tag count
    const bl = Uint32Array.BYTES_PER_ELEMENT * 2 + Uint8Array.BYTES_PER_ELEMENT;
    const tagNumberDataView = new DataView(buffer, bl, Uint16Array.BYTES_PER_ELEMENT);
    tagNumberDataView.setUint16(0, tagCount, false);
    return buffer;
  }

  /**
   * The first request trigger a 8 bytes number to be associate with the
   * session (the salt number).
   */
  private getAuthRequest1(): ArrayBuffer {
    this._setHeadersToRequest(2); // EC_OP_AUTH_REQ
    let tagCount = 0;
    this._buildTagArrayBuffer(this.ECTagNames.EC_TAG_CLIENT_NAME * 2, this.ECOpCodes.EC_OP_STRINGS, 'amule-js\0', null);
    tagCount++;
    this._buildTagArrayBuffer(this.ECTagNames.EC_TAG_CLIENT_VERSION * 2, this.ECOpCodes.EC_OP_STRINGS, '1.0\0', null);
    tagCount++;
    this._buildTagArrayBuffer(4, this.ECOpCodes.EC_TAGTYPE_UINT16, this.ProtocolVersion.EC_CURRENT_PROTOCOL_VERSION, null);
    tagCount++;
    return this._finalizeRequest(tagCount);
  }

  /**
   * When the solt number (aka session id) is given by the server, we can auth
   */
  private _getAuthRequest2(): ArrayBuffer {
    this._setHeadersToRequest(80);
    let tagCount = 0;
    const passwd = this.md5(this.md5Password + this.md5(this.solt));
    this._buildTagArrayBuffer(2, this.ECOpCodes.EC_TAGTYPE_HASH16, passwd, null);
    tagCount++;
    return this._finalizeRequest(tagCount);
  }

  /**
   *  < EC_OP_SEARCH_START opCode:38 size:38 (compressed: 30)
   *      EC_TAG_SEARCH_TYPE tagName:1793 dataType:2 dataLen:1 = EC_SEARCH_LOCAL
   *        EC_TAG_SEARCH_NAME tagName:1794 dataType:6 dataLen:10 = keywords 2017
   *        EC_TAG_SEARCH_FILE_TYPE tagName:1797 dataType:6 dataLen:1 =
   *  > EC_OP_STRINGS opCode:6 size:59 (compressed: 54)
   *      EC_TAG_STRING tagName:0 dataType:6 dataLen:49 = Search in progress. Refetch results in a moment!
   *
   * or
   *
   *  < EC_OP_SEARCH_START opCode:38 size:38 (compressed: 30)
   *      EC_TAG_SEARCH_TYPE tagName:1793 dataType:2 dataLen:1 = EC_SEARCH_KAD
   *        EC_TAG_SEARCH_NAME tagName:1794 dataType:6 dataLen:10 = keywords 2017
   *        EC_TAG_SEARCH_FILE_TYPE tagName:1797 dataType:6 dataLen:1 =
   *
   * > EC_OP_FAILED opCode:5 size:61
   *    EC_TAG_STRING tagName:0 dataType:6 dataLen:51 = eD2k search can't be done if eD2k is not connected
   */
  private _getSearchStartRequest(q: string, searchType): ArrayBuffer {
    this._setHeadersToRequest(this.ECOpCodes.EC_OP_SEARCH_START); // 38
    let tagCount = 0;
    const children = [
      {
        ecTag: 1794 * 2,
        ecOp: this.ECOpCodes.EC_OP_STRINGS, // 6
        value: q + '\0',
      },
      {
        ecTag: this.EC_TAG_SEARCHFILE.EC_TAG_SEARCH_FILE_TYPE, // 1797*2
        ecOp: this.ECOpCodes.EC_OP_STRINGS,
        value: '\0',
      },
      {
        ecTag: this.EC_TAG_SEARCHFILE.EC_TAG_SEARCH_EXTENSION,
        ecOp: this.ECOpCodes.EC_OP_STRINGS,
        value: 'mp4\0',
      },
    ];

    // this.EC_SEARCH_TYPE.EC_SEARCH_KAD EC_SEARCH_LOCA
    this._buildTagArrayBuffer(this.EC_TAG_SEARCHFILE.EC_TAG_SEARCH_TYPE, this.ECOpCodes.EC_TAGTYPE_UINT8, searchType, children);
    tagCount++;
    return this._finalizeRequest(tagCount);
  }

  /**
   * < EC_OP_SEARCH_PROGRESS opCode:41 size:3 (compressed: 2)
   * > EC_OP_SEARCH_PROGRESS opCode:41 size:11 (compressed: 8)
   *     EC_TAG_SEARCH_STATUS tagName:1800 dataType:2 dataLen:1 = 0
   */
  private _isSearchFinished(): ArrayBuffer {
    this._setHeadersToRequest(this.ECOpCodes.EC_OP_SEARCH_PROGRESS); // 41
    return this._finalizeRequest(0);
  }

  /**
   *
   */
  private getSharedFilesRequest(): ArrayBuffer {
    this._setHeadersToRequest(this.ECCodes.EC_OP_GET_SHARED_FILES);
    return this._finalizeRequest(0);
  }

  /**
   *
   */
  private getSearchResultRequest(): ArrayBuffer {
    this._setHeadersToRequest(this.ECOpCodes.EC_OP_SEARCH_RESULTS);
    let tagCount = 0;
    this._buildTagArrayBuffer(8, this.ECOpCodes.EC_TAGTYPE_UINT8, this.EC_SEARCH_TYPE.EC_SEARCH_LOCA, null);
    tagCount++;
    return this._finalizeRequest(tagCount);
  }

  private getDownloadsRequest(): ArrayBuffer {
    this._setHeadersToRequest(13); // EC_OP_GET_DLOAD_QUEUE
    return this._finalizeRequest(0);
  }

  /**
   *  < EC_OP_DOWNLOAD_SEARCH_RESULT opCode:42 size:36 (compressed: 28)
   *      EC_TAG_PARTFILE tagName:768 dataType:9 dataLen:16 = 26E4413971DF1EC89AC3B91A4A02402F
   *        EC_TAG_PARTFILE_CAT tagName:783 dataType:2 dataLen:1 = 0
   *  > EC_OP_STRINGS opCode:6 size:3 (compressed: 2)
   */
  private downloadRequest(e: AmuleResponse): ArrayBuffer {
    this._setHeadersToRequest(this.ECOpCodes.EC_OP_DOWNLOAD_SEARCH_RESULT); // 42
    let tagCount = 0;
    const children = [
      {
        ecTag: 783 * 2,
        ecOp: this.ECOpCodes.EC_TAGTYPE_UINT8, // 2
        value: 0,
      },
    ];

    // if has children => +1
    this._buildTagArrayBuffer(768 * 2 + 1, this.ECOpCodes.EC_TAGTYPE_HASH16, e.partfile_hash, children);
    tagCount++;
    return this._finalizeRequest(tagCount);
  }

  /**
   * < EC_OP_SET_PREFERENCES opCode:64 size:20 (compressed: 14)
   *     EC_TAG_PREFS_CONNECTIONS tagName:4864 dataType:1 dataLen:0 = empty
   *       EC_TAG_CONN_MAX_DL tagName:4867 dataType:2 dataLen:1 = 0
   * or
   *  < EC_OP_SET_PREFERENCES opCode:64 size:20 (compressed: 14)
   *     EC_TAG_PREFS_CONNECTIONS tagName:4864 dataType:1 dataLen:0 = empty
   *       EC_TAG_CONN_MAX_UL tagName:4868 dataType:2 dataLen:1 = 0
   */
  private getSetMaxBandwithRequest(tag: number, limit: number): ArrayBuffer {
    this._setHeadersToRequest(64);
    let tagCount = 0;
    const children = [
      {
        ecTag: tag * 2,
        ecOp: this.ECOpCodes.EC_TAGTYPE_UINT8, // 2
        value: limit,
      },
    ];

    // if has children => +1
    this._buildTagArrayBuffer(4864 * 2 + 1, 1, null, children);
    tagCount++;
    return this._finalizeRequest(tagCount);
  }

  /**
   *
   */
  private simpleRequest(opCode: number): ArrayBuffer {
    this._setHeadersToRequest(opCode);
    return this._finalizeRequest(0);
  }

  /**
   * EC_OP_STAT_REQ == 10 for a short summary
   * EC_OP_GET_UPDATE == 82  for the list of dl and ul files
   *
   * < EC_OP_STAT_REQ opCode:10 size:11 (compressed: 6)
   *     EC_TAG_DETAIL_LEVEL tagName:4 dataType:2 dataLen:1 = EC_DETAIL_INC_UPDATE
   * > EC_OP_STATS opCode:12 size:316 (compressed: 215)
   *     EC_TAG_STATS_UP_OVERHEAD tagName:516 dataType:2 dataLen:1 = 197
   *     EC_TAG_STATS_DOWN_OVERHEAD tagName:517 dataType:2 dataLen:1 = 164
   *     EC_TAG_STATS_BANNED_COUNT tagName:519 dataType:2 dataLen:1 = 0
   *     ...
   */
  private getStatsRequest(EC_OP = 10): ArrayBuffer {
    this._setHeadersToRequest(EC_OP); // EC_OP_STAT_REQ == 10
    let tagCount = 0;
    const EC_TAG_DETAIL_LEVEL = 4;
    const EC_DETAIL_INC_UPDATE = 4;
    this._buildTagArrayBuffer(EC_TAG_DETAIL_LEVEL * 2, this.ECOpCodes.EC_TAGTYPE_UINT8, EC_DETAIL_INC_UPDATE, null);
    tagCount++;
    return this._finalizeRequest(tagCount);
  }

  /**
   *  < EC_OP_GET_PREFERENCES opCode:63 size:20 (compressed: 13)
   *      EC_TAG_DETAIL_LEVEL tagName:4 dataType:2 dataLen:1 = EC_DETAIL_UPDATE
   *      EC_TAG_SELECT_PREFS tagName:4096 dataType:3 dataLen:2 = 15103
   *  > EC_OP_SET_PREFERENCES opCode:64 size:683 (compressed: 540)
   *      EC_TAG_DETAIL_LEVEL tagName:4 dataType:2 dataLen:1 = EC_DETAIL_UPDATE
   *      EC_TAG_PREFS_GENERAL tagName:4608 dataType:1 dataLen:0 = empty
   *        EC_TAG_USER_NICK tagName:4609 dataType:6 dataLen:13 = name
   */
  private getPreferencesRequest(): ArrayBuffer {
    this._setHeadersToRequest(63);
    let tagCount = 0;
    const EC_TAG_DETAIL_LEVEL = 4;
    const EC_DETAIL_UPDATE = 3;
    this._buildTagArrayBuffer(EC_TAG_DETAIL_LEVEL * 2, this.ECOpCodes.EC_TAGTYPE_UINT8, EC_DETAIL_UPDATE, null);
    tagCount++;
    this._buildTagArrayBuffer(4096 * 2, this.ECOpCodes.EC_TAGTYPE_UINT16, 15103, null);
    tagCount++;
    return this._finalizeRequest(tagCount);
  }

  /**
   * < EC_OP_PARTFILE_DELETE opCode:29 size:26 (compressed: 22)
   *     EC_TAG_PARTFILE tagName:768 dataType:9 dataLen:16 = EA63C3774DF2EB871EFA3AC58543B66F
   */
  private getCancelDownloadRequest(e): ArrayBuffer {
    this._setHeadersToRequest(29); // EC_OP_PARTFILE_DELETE
    this._buildTagArrayBuffer(768 * 2, this.ECOpCodes.EC_TAGTYPE_HASH16, e.partfile_hash, null);
    return this._finalizeRequest(1);
  }

  private readSalt(buffer): number {
    let offset: number = Uint32Array.BYTES_PER_ELEMENT * 2;
    const dataView: DataView = new DataView(buffer, offset, Uint8Array.BYTES_PER_ELEMENT);
    this.responseOpcode = dataView.getUint8(0);
    // console.log("response opcode : " + this.responseOpcode);
    offset = offset + Uint8Array.BYTES_PER_ELEMENT;
    // console.log("response tag count : " + dataView.getUint16(0, false));
    offset = offset + Uint16Array.BYTES_PER_ELEMENT;
    // console.log("response tag # name (ecTag) : " + dataView.getUint16(0, false));
    offset = offset + Uint16Array.BYTES_PER_ELEMENT;
    // console.log("response tag # type (ecOp): " + dataView.getUint8(0, false));
    offset = offset + Uint8Array.BYTES_PER_ELEMENT;
    // console.log("response tag # length : " + dataView.getUint32(0, false));
    offset = offset + Uint32Array.BYTES_PER_ELEMENT;

    if (this.responseOpcode === 79) {
      const dv: DataView = new DataView(buffer, offset, 8); // 8 bytes
      for (let i = 0; i < 8; i++) {
        let c: string = dv
          .getUint8(i)
          .toString(16)
          .toUpperCase();
        if (c.length < 2 && i !== 0) {
          c = '0' + c;
        }
        this.solt += c;
      }
      offset = offset + 8;
    }
    return this.responseOpcode;
  }

  private readBuffer(buffer: ArrayBuffer, byteNumberToRead: number, littleEndian = false): number {
    let val = null;
    const dataView = new DataView(buffer, this.offset, byteNumberToRead);
    if (byteNumberToRead === 1) {
      val = dataView.getUint8(0);
    } else if (byteNumberToRead === 2) {
      val = dataView.getUint16(0, littleEndian);
    } else if (byteNumberToRead === 4) {
      val = dataView.getUint32(0, littleEndian);
    }
    this.offset += byteNumberToRead;
    return val;
  }

  private readBufferChildren(buffer: ArrayBuffer, res: AmuleResponse, recursivity = 1): AmuleResponse {
    res.children = [];

    for (let j = 0; j < res.tagCountInResponse; j++) {
      const child = new AmuleResponse();
      child.nameEcTag = this.readBuffer(buffer, 2);
      child.typeEcOp = this.readBuffer(buffer, 1);
      child.length = this.readBuffer(buffer, 4); // length without ectag, ecOp, length and tag count BUT with children length
      child.tagCountInResponse = 0;
      child.value = '';

      res.length -= 7 + child.length; // remove header length + length

      if (child.nameEcTag % 2) {
        // if name (ecTag) is odd there is a child count
        child.nameEcTag = (child.nameEcTag - 1) / 2;
        child.tagCountInResponse = this.readBuffer(buffer, 2);
        res.length -= 2;
      } else {
        child.nameEcTag = child.nameEcTag / 2;
      }
      this.readBufferChildren(buffer, child, recursivity + 1);
      res.children.push(child);
    }
    if (recursivity > 1) {
      res.value = this.readValueOfANode(res, buffer);
    }
    return res;
  }

  /**
   * Read the value of a node according to its type (typeEcOp) and size in the buffer
   */
  private readValueOfANode(child2: AmuleResponse, buf: ArrayBuffer) {
    if (!child2.length) {
      return '';
    }
    if (child2.typeEcOp === this.ECOpCodes.EC_TAGTYPE_UINT8) {
      // 2
      child2.value = this.readBuffer(buf, child2.length);
    } else if (child2.typeEcOp === this.ECOpCodes.EC_TAGTYPE_UINT16) {
      // 3
      child2.value = this.readBuffer(buf, child2.length);
    } else if (child2.typeEcOp === 4) {
      // integer
      child2.value = this.readBuffer(buf, child2.length);
    } else if (child2.typeEcOp === 5) {
      // integer 8 bytes
      const high = this.readBuffer(buf, 4);
      const low = this.readBuffer(buf, 4);
      let n = high * 4294967296.0 + low;
      if (low < 0) {
        n += 4294967296;
      }
      child2.value = n;
    } else if (child2.typeEcOp === this.ECOpCodes.EC_OP_STRINGS) {
      // 6
      if (!this.textDecoder && typeof this.stringDecoder === 'undefined') {
        console.log('you won t be able to read special utf-8 char');
      }
      let uint8array: number[] = [];
      for (let m1 = 0; m1 < child2.length; m1++) {
        const intValue = this.readBuffer(buf, 1);
        if (intValue > 0x80) {
          // wired utf-8 char
          uint8array.push(intValue);
        } else if (uint8array.length > 0) {
          // end of wired utf-8 char
          if (this.textDecoder) {
            // browser
            child2.value += this.textDecoder.decode(new Uint8Array(uint8array));
          } else if (this.stringDecoder) {
            // nodeJs
            child2.value += this.stringDecoder.write(Buffer.from(uint8array));
          }
          uint8array = [];
          child2.value += '' + String.fromCharCode(intValue); // work for all javascript engine
        } else {
          child2.value += '' + String.fromCharCode(intValue);
        }
      }
      if (child2.value) {
        child2.value = child2.value.substring(0, child2.value.length - 1);
      }
    } else if (child2.typeEcOp === this.ECOpCodes.EC_TAGTYPE_HASH16) {
      // 9
      for (let m3 = 0; m3 < child2.length; m3 = m3 + 2) {
        let c = this.readBuffer(buf, 2).toString(16);
        c = ('0000' + c).slice(-4);
        child2.value += c;
      }
      if (child2.value.length !== 32) {
        console.log('HASH is false: ' + child2.value + ' -- ' + +child2.value.length);
      }
    } else {
      // console.log('WARNING: not read : child2.typeEcOp = ' + child2.typeEcOp);
      // TODO
      // wrong but we do it to read the buffer
      for (let m = 0; m < child2.length; m++) {
        child2.value += '' + this.readBuffer(buf, 1);
      }
    }
    return child2.value;
  }

  private _readHeader(buffer: ArrayBuffer): AmuleResponse {
    this.offset = 0;
    const response = new AmuleResponse();
    response.header = this.readBuffer(buffer, 4);
    // length (total minus header and response Length)
    const responseLength = this.readBuffer(buffer, 4);
    response.totalSizeOfRequest = responseLength + 8;
    // children response length (total minus header, response length, opcode, tag count)
    response.length = responseLength - 3;
    response.opCode = this.readBuffer(buffer, 1);
    response.tagCountInResponse = this.readBuffer(buffer, 2);
    return response;
  }

  private _formatResultsList(response: AmuleResponse): AmuleResponse {
    response.children.map(e => {
      this.EC_TAG_MAPPING.map(REF => {
        Object.keys(REF).map(key => {
          if (!e.children.length && e.nameEcTag === REF[key]) {
            response[key.split('EC_TAG_')[1].toLowerCase()] = e.value;
          }
        });
      });
      if (!e.length) {
        delete e.value;
      }
      delete e.length;
      delete e.tagCountInResponse;
      if (e.children && !e.children.length) {
        delete e.children;
      } else {
        this._formatResultsList(e);
      }
    });
    this.EC_TAG_MAPPING.map(REF => {
      Object.keys(REF).map(key => {
        if (response.nameEcTag === REF[key]) {
          response['label'] = key.split('EC_TAG_')[1].toLowerCase();
        }
      });
    });
    if (response['knownfile_xferred_all']) {
      response['sharedRatio'] = this._floor(response['knownfile_xferred_all'] / response['partfile_size_full']);
    }
    if (response['partfile_size_done']) {
      response['completeness'] = this._floor(response['partfile_size_done'] / response['partfile_size_full']);
    }
    return response;
  }

  private _floor = a => Math.floor(a * 100) / 100;

  private readResultsList(buffer: ArrayBuffer): AmuleResponse {
    const response = this._readHeader(buffer);
    switch (response.opCode) {
      case 1:
        response.opCodeLabel = 'EC_OP_NOOP';
        break;
      case 5:
        response.opCodeLabel = 'EC_OP_FAILED';
        break;
      case 31:
        response.opCodeLabel = 'EC_OP_DLOAD_QUEUE';
        break;
      case 40:
        response.opCodeLabel = 'EC_OP_SEARCH_RESULTS';
        break;
      default:
    }
    this.readBufferChildren(buffer, response);
    this._formatResultsList(response);
    return response;
  }

  private toBuffer(ab: ArrayBuffer): Buffer {
    return Buffer.from(new Uint8Array(ab));
  }

  private toArrayBuffer(buf: ArrayBuffer): ArrayBuffer | SharedArrayBuffer {
    return new Uint8Array(buf).buffer;
  }

  /**
   * Create a TCP socket with the server.
   */
  private initConnToServer(host: string, port: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.client = net.createConnection(port, host, resolve);
      this.client.once('error', reject);
    });
  }

  private sendToServer_simple(data: ArrayBuffer): Promise<ArrayBuffer | SharedArrayBuffer> {
    return new Promise((resolve, reject) => {
      this.client.write(this.toBuffer(data));
      this.client.once('data', d => resolve(this.toArrayBuffer(d)));
    });
  }

  private sendToServer(data: ArrayBuffer, err?: string): Promise<AmuleResponse> {
    return new Promise<ArrayBuffer>((resolve, reject) => {
      const buf = [];
      const frequency = 100,
        timeout = 200;
      let totalSizeOfRequest = frequency,
        count = 0;

      this.client.write(this.toBuffer(data));
      this.client.on('data', d => buf.push(this.toArrayBuffer(d)));

      const intervalId = setInterval(() => {
        if (buf[0]) {
          totalSizeOfRequest = this._readHeader(buf[0]).totalSizeOfRequest;
          let bl = 0;
          buf.forEach(b => {
            bl += b.byteLength;
          });
          if (bl >= totalSizeOfRequest) {
            const buffer = new ArrayBuffer(bl);
            let o = 0;
            buf.forEach(b => {
              for (let j = 0; j < b.byteLength; j++) {
                const fromArrayView = new Uint8Array(b, j, 1);
                const toArrayView = new Uint8Array(buffer, j + o, 1);
                toArrayView.set(fromArrayView);
              }
              o = o + b.byteLength;
            });
            clearInterval(intervalId);
            if (this.client) {
              this.client.removeAllListeners('data');
            }
            resolve(buffer);
          }
        }
        if (count++ > timeout) {
          clearInterval(intervalId);
          reject('time out for this TCP request. ' + err);
        }
      }, frequency);
    }).then(d => this.readResultsList(d));
  }

  /**
   * Connect to aMule server. If already connected, return a resolved promise but don't connect again.
   */
  public connect(): Promise<string> {
    if (this.isConnected) {
      return Promise.resolve('already connected');
    }
    return this.initConnToServer(this.ip, this.port)
      .then(() => {
        return this.sendToServer_simple(this.getAuthRequest1());
      })
      .then(data => {
        this.readSalt(data);
        return this.sendToServer_simple(this._getAuthRequest2());
      })
      .then(data => {
        if (this.readSalt(data) === 4) {
          this.isConnected = true;
          return 'You are successfuly connected to amule';
        } else {
          throw new Error('You are NOT connected to amule');
        }
      })
      .catch(err => {
        throw new Error('\n\nYou are NOT connected to amule: ' + err);
      });
  }

  private filterResultList(list: AmuleResponse, q: string, strict: boolean): AmuleResponse[] {
    if (strict && list.children) {
      list.children = list.children.filter(e => {
        let isPresent = true;
        const fileName = e['partfile_name'];
        q.split(' ').map(r => {
          if (fileName && fileName.toLowerCase().indexOf(r.toLowerCase()) === -1) {
            isPresent = false;
          }
        });
        return isPresent;
      });
    }
    return list.children;
  }

  /**
   * Lauch a search on a server (kad or edonkey)
   */
  public async __search(q: string, network: number, strict = true): Promise<AmuleResponse[]> {
    q = q.trim();
    const res = await this.sendToServer(this._getSearchStartRequest(q, network), q);
    return new Promise<AmuleResponse[]>((resolve, reject) => {
      if (network === this.EC_SEARCH_TYPE.EC_SEARCH_KAD) {
        const timeout = 120,
          frequency = 2000;
        let isSearchFinished = false,
          count = 0;
        const intervalId = setInterval(() => {
          if (isSearchFinished) {
            clearInterval(intervalId);
            this.fetchSearch().then(list => resolve(this.filterResultList(list, q, strict)));
          }
          this.sendToServer(this._isSearchFinished()).then(r => {
            if (r.children && r.children[0] && r.children[0].value !== 0) {
              isSearchFinished = true;
            }
          });
          if (count++ > timeout) {
            console.error('time out expired to fetch result');
            clearInterval(intervalId);
          }
        }, frequency);
      } else if (network === this.EC_SEARCH_TYPE.EC_SEARCH_LOCA) {
        // TODO to improve
        setTimeout(() => {
          this.fetchSearch().then(list_2 => resolve(this.filterResultList(list_2, q, strict)));
        }, 1500);
      } else {
        reject('network not known');
      }
    });
  }

  /**
   * Remove wrong charaters
   */
  public _clean(str: string) {
    return str
      .replace(new RegExp('Ã©', 'g'), 'e')
      .replace(new RegExp('�', 'g'), 'A')
      .replace(new RegExp('Ã¨', 'g'), 'e');
  }

  /**
   * Perform a search on the amule server.
   * On each result currentDl == true if already in the download queue, downloaded == true if already downloaded
   *
   * @param q search words
   * @param network kat or edonkey
   */
  public async search(q: string, network: number = this.EC_SEARCH_TYPE.EC_SEARCH_KAD): Promise<AmuleResponse[]> {
    const allFiles = await this.getSharedFiles();
    const dlFiles = await this.getDownloads();
    const list = await this.__search(q, network);
    allFiles.map(f => list.map(s => (s.partfile_hash === f.partfile_hash ? (s.downloaded = true) : false)));
    dlFiles.map(f => list.map(s => (s.partfile_hash === f.partfile_hash ? (s.currentDl = true) : false)));
    list.map(e => (e.partfile_name = this._clean(e.partfile_name)));
    return list.sort((a, b) => b.partfile_source_count - a.partfile_source_count);
  }

  public fetchSearch(): Promise<AmuleResponse> {
    return this.sendToServer(this.getSearchResultRequest());
  }

  /**
   * get all the files being currently downloaded
   */
  public async getDownloads(): Promise<AmuleResponse[]> {
    const elements = await this.sendToServer(this.getDownloadsRequest(), 'getDownloads request');
    if (elements.children) {
      elements.children.map(f => {
        ['partfile_last_recv', 'partfile_last_seen_comp'].map(key => {
          if (f[key]) {
            f[key + '_f'] = new Date(f[key] * 1000);
          }
        });
        ['partfile_speed', 'completeness'].map(key => {
          if (!f[key]) {
            f[key] = 0;
          }
        });
        delete f.children;
      });
    }
    return elements.children.sort((a, b) => b.completeness - a.completeness);
  }

  /**
   * download a file in the search list
   *
   * @param e file to download (must have a hash)
   */
  public download(e: AmuleResponse): Promise<any> {
    return this.sendToServer_simple(this.downloadRequest(e));
  }

  /**
   * return the list of shared files
   */
  public async getSharedFiles(): Promise<AmuleResponse[]> {
    const elements = await this.sendToServer(this.getSharedFilesRequest(), 'getSharedFiles request');
    elements.children.map(f => {
      ['knownfile_req_count_all', 'sharedRatio'].map(key => {
        if (!f[key]) {
          f[key] = 0;
        }
      });
      delete f.children;
      // remove files being currently downloaded
      if (f['knownfile_filename'] && f['knownfile_filename'].endsWith('.part')) {
        const index = elements.children.indexOf(f);
        elements.children.splice(index, 1);
      }
      f.downloaded = true;
    });
    return elements.children.sort((a, b) => b.sharedRatio - a.sharedRatio);
  }

  /**
   *
   */
  public getDetailUpdate(): Promise<AmuleResponse> {
    return this.sendToServer(this.getStatsRequest(82));
  }

  /**
   * EC_OP_CLEAR_COMPLETED
   */
  public clearCompleted(): Promise<any> {
    return this.sendToServer_simple(this.simpleRequest(0x53));
  }
  public getStatistiques(): Promise<AmuleResponse> {
    return this.sendToServer(this.getStatsRequest(10));
  }
  public setMaxDownload(limit): Promise<any> {
    return this.sendToServer_simple(this.getSetMaxBandwithRequest(4867, limit));
  }
  public setMaxUpload(limit): Promise<any> {
    return this.sendToServer_simple(this.getSetMaxBandwithRequest(4868, limit));
  }
  public cancelDownload(e): Promise<any> {
    return this.sendToServer_simple(this.getCancelDownloadRequest(e));
  }

  /**
   * get user preferences (EC_OP_GET_PREFERENCES)
   */
  public getPreferences(): Promise<AmuleResponse> {
    return this.sendToServer(this.getPreferencesRequest());
  }

  /**
   * reload shared files list (EC_OP_SHAREDFILES_RELOAD)
   */
  public reloadSharedFiles(): Promise<any> {
    return this.sendToServer_simple(this.simpleRequest(35));
  }

  /**
   * get the date. Example: "2017-11"
   */
  public getMonth(): string {
    const dateObj = new Date(),
      month = dateObj.getUTCMonth() + 1,
      monthStr: string = ('00' + month).slice(-2),
      year: number = dateObj.getUTCFullYear();
    return year + '-' + monthStr;
  }

  /**
   * @param funcs array of function with promises to execute
   */
  public promiseSerial<T>(funcs: Array<() => Promise<T>>) {
    return funcs.reduce((promise, func) => promise.then(result => func().then(x => result.concat(x))), Promise.resolve<T[]>([]));
  }

  /**
   * parse the files and get the most commons terms
   * @param list of files
   * @param limit number of results wanted
   * @param minLength of the words
   */
  public getMostUsedWord(list: AmuleResponse[], limit = 10, minLength = 4): string[] {
    class Occurence {
      name: string;
      occurence = 1;
      constructor(name: string) {
        this.name = name;
      }
    }
    const regex = new RegExp('[ \\[\\]()_.-]');
    const count: Occurence[] = [];
    list.map(e => {
      const splited = e.partfile_name.split(regex);
      splited.pop(); // remove the file extension
      splited
        .filter(a => a.length >= minLength)
        .map(ele => {
          const occ = count.filter(b => b.name === ele);
          if (occ.length > 0) {
            occ[0].occurence++;
          } else {
            count.push(new Occurence(ele));
          }
          return ele;
        });
    });
    count.sort((a, b) => b.occurence - a.occurence);
    return count.slice(0, limit).map(e => e.name);
  }
}
