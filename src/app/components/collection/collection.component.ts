import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AmuleService } from '../../providers/amule.service';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AmuleResponse } from '../../amule';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  displayedColumns = ['partfile_name', 'sharedRatio', 'partfile_size_full', 'firstSeenDl'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @Input()
  set results(results: AmuleResponse[]) {
    this.dataSource.data = results;
  }

  constructor(public service: AmuleService) {
  }

  ngOnInit(): void {
    this.sort.sort(<MatSortable>({ id: 'firstSeen', start: 'desc' }));
    this.dataSource.sort = this.sort;
  }

  open = e => this.service.open(e);

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
