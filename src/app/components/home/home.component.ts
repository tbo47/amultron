import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { AmuleService, AmuleData, AmultronAuth } from '../../providers/amule.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  data: AmuleData; // data structure with all files
  error: string;
  auth: AmultronAuth;
  isLoading = false;

  constructor(public service: AmuleService) { }

  update = () => {
    this.service.getLists(this.data).subscribe(l => this.data = l);
  }

  ngOnInit(): void {
    this.service.getAuthFromCache().subscribe(p => {
      this.auth = p;
      if (p.autoconnect) {
        this.isLoading = true;
        this.onConnect();
      }
    });
  }

  onConnect() {
    this.service.create(this.auth);
    this.service.setAuthInCash(this.auth);
    this.service.getLists(this.data).subscribe(l => {
      this.data = l;
      this.isLoading = false;
      interval(2 * 60 * 1000).subscribe(() => this.update());
    });
  }

}
