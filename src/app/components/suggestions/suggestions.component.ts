import { Component, OnInit, Input } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatIconRegistry } from '@angular/material/icon';
import { AmuleService, Suggestion } from '../../providers/amule.service';
import { AmuleResponse } from '../../amule';
import { DomSanitizer } from '@angular/platform-browser';
import { interval } from 'rxjs';

@Component({
  selector: 'app-suggestions',
  templateUrl: './suggestions.component.html',
  styleUrls: ['./suggestions.component.scss']
})
export class SuggestionsComponent implements OnInit {

  isSearching = false;
  visible = true;
  selectable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  suggestions: Suggestion[] = [];
  displayedColumns = ['partfile_name', 'partfile_source_count', 'partfile_size_full', 'firstSeen'];

  constructor(public service: AmuleService, iconR: MatIconRegistry, sanitizer: DomSanitizer) {
    iconR.addSvgIcon('cancel', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/baseline-cancel-24px.svg'));
    iconR.addSvgIcon('clear', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/ic_clear_black_48px.svg'));
    iconR.addSvgIcon('remove', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/ic_clear_black_48px.svg'));
  }

  ngOnInit() {
    interval(2 * 60 * 60 * 1000).subscribe(this._triggerSugs);
  }

  @Input()
  set results(results: AmuleResponse[]) {
    if (!this.suggestions.length && results.length) {
      this.service.getSuggestions(results).subscribe(s => {
        this.suggestions = s;
        this._triggerSugs();
      });
    }
  }

  private _triggerSugs() {
    if (this.suggestions.length) {
      this.isSearching = true;
      console.log('suggestions started');
      this.service.triggerSuggestionSearch(this.suggestions, 2).subscribe(sugs => {
        sugs.map(s => s.shortList = s.results.slice(0, 10));
        this.suggestions = sugs;
        this.isSearching = false;
        this.service.setSuggestionsInCash(this.suggestions);
        console.log(this.suggestions);
        console.log('suggestions finished');
      }, err => {
        this.isSearching = false;
        console.error(Date().toString() + err);
      });
    }
  }

  suggest(): void {
    this._triggerSugs();
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.suggestions.push(new Suggestion(value.trim()));
    }
    if (input) {
      input.value = '';
    }
    this.service.setSuggestionsInCash(this.suggestions);
  }

  remove(sug: Suggestion): void {
    const index = this.suggestions.indexOf(sug);
    if (index >= 0) {
      this.suggestions.splice(index, 1);
    }
    this.service.setSuggestionsInCash(this.suggestions);
  }

  download = e => this.service.download(e);
}
