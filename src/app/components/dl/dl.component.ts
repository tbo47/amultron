import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AmuleResponse } from '../../amule';
import { AmuleService } from '../../providers/amule.service';

@Component({
  selector: 'app-dl',
  templateUrl: './dl.component.html',
  styleUrls: ['./dl.component.scss']
})
export class DlComponent implements OnInit {

  displayedColumns = ['partfile_name', 'partfile_size_full', 'completeness'];
  dataSource = new MatTableDataSource([]);
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @Input()
  set items(results: AmuleResponse[]) {
    this.dataSource.data = results;
  }

  constructor(public service: AmuleService) {
  }

  ngOnInit(): void {
    this.sort.sort(<MatSortable>({ id: 'completeness', start: 'desc' }));
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
