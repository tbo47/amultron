import { Component, Output, EventEmitter, ViewChild, OnInit } from '@angular/core';
import { AmuleService } from '../../providers/amule.service';
import { MatIconRegistry } from '@angular/material/icon';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DomSanitizer } from '@angular/platform-browser';
import { EdkFile } from '../../providers/file';
import { sortBy } from 'lodash';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  displayedColumns: string[] = ['partfile_name', 'partfile_source_count', 'partfile_size_full', 'firstSeen'];
  q = '';
  dataSource = new MatTableDataSource([]);

  itemsAll: EdkFile[] = [];
  isSearching = false; // is a search pending
  showAdvance = false; // show advanced parameters
  network = 2;
  networks = [{ label: 'kad', value: 2 }, { label: 'edonkey', value: 0 }];
  @Output() refresh = new EventEmitter<boolean>(); // to notify to refresh the dl list
  error: string;
  showDl = true; // show already downloaded files in the results list
  showSearch = false; // show the search bar
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(public service: AmuleService, iconR: MatIconRegistry, sanitizer: DomSanitizer) {
    iconR.addSvgIcon('search', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/ic_search_black_48px.svg'));
    iconR.addSvgIcon('visibility', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/ic_visibility_black_48px.svg'));
    iconR.addSvgIcon('visibility_off', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/ic_visibility_off_black_48px.svg'));
    iconR.addSvgIcon('clear', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/ic_clear_black_48px.svg'));
  }

  ngOnInit() {
    this.sort.sort(<MatSortable>{ id: 'firstSeen', start: 'desc' });
    this.dataSource.sort = this.sort;
    this.service.getDefaultSearch().subscribe(q => (this.q = q));
  }

  search = () => {
    this.service.setDefaultSearch(this.q);
    this.itemsAll = []; // TODO remove this and rely on dataSource
    this.isSearching = true;
    this.error = '';
    this.service.search(this.q, this.network).subscribe(
      list => {
        this.itemsAll = list;
        this.dataSource.data = this._filter(this.itemsAll);
        this.isSearching = false;
        console.log(this.dataSource.data);
      },
      err => {
        this.error = err;
        this.isSearching = false;
      },
    );
  };

  download = e => this.service.download(e).then(() => this.refresh.emit(true));

  private _filter(list: EdkFile[]): EdkFile[] {
    return list.filter(e => this.showDl || !(e.downloaded || e.currentDl));
  }

  toggleList(isShow: boolean) {
    this.showDl = isShow;
    if (isShow) {
      this.service.snack('Show all results.');
    } else {
      this.service.snack('Hide results already downloaded.');
    }
    this.dataSource.data = this._filter(this.itemsAll);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
