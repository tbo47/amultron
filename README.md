Electron client for amule. It's not meant to be a replacement for amulegui but rather add features like suggestions or highlights of newly downloaded files.

## Requirements

Install nodejs, npm.

Make sure amule is configure to accept external connections. So ~/.aMule/amule.conf should look like:

```
...
AcceptExternalConnections=1
...
ECPassword=5f4dcc3b5aa765d61d8327deb882cf99
...
```

## Electron app

Clone this repository:

``` bash
git clone git@gitlab.com:lasthib/amultron.git
```

Install dependencies with npm :

``` bash
npm install
```

Run the electron app.

``` bash
npm run electron:local
```

Compile for mac

``` bash
npm run electron:mac
```

Compile for linux

``` bash
npm run electron:linux
```

Compile for windows

``` bash
npm run electron:windows
```

## Nodejs scripts

Interested in creating your own javascript app? Here are very simple examples to understand how it all works.

* [show-shared-file.ts](bin/show-shared-file.ts) to list the shared files
* [search.ts](bin/search.ts) to trigger all downloads of a search result
* [export.ts](bin/export.ts) to export shared files list in html format